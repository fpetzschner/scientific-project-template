# README #

This repository contains a potential structure for a scientific project.
The research we are conducting is in Neuroscience, so some specifics are optimised for that.

It contains:

* the possible folders and subfolders and their documentation
* the planning steps
* suggestions for coding
* a labbook template in tex

### Who do I talk to? ###

* Repo owner: Frederike H. Petzschner
* contact: petzschner@biomed.ee.ethz.ch
* suggestions are welcome!